# A general scaffold for laravel 5.1/angular/android/iOS projects

* You will require composer for the laravel api.
* Node npm is required to install bower and gulp.
* In order for your IDE to use the .editorconfig you might require a plugin for your IDE (http://editorconfig.org/)

## Instructions:

* remove the git upstream and set to your own repo:
  - you can either simply delete the .git directory or 
    - git remote -v
    - git remote rm {remote}
    - git remote -v