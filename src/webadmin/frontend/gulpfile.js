'use strict';

var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    browserify = require('gulp-browserify'),
    concat = require('gulp-concat'),
    del = require('del'),
    autoprefixer = require('gulp-autoprefixer'),
    jade = require('gulp-jade'),
    stylus = require('gulp-stylus'),
    minifyCSS;

// Modules for webserver and livereload
var express = require('express'),
    refresh = require('gulp-livereload'),
    livereload = require('connect-livereload'),
    livereloadport = 35729,
    serverport = 5000;

// Set up an express server (not starting it yet)
var server = express();
// Add live reload
server.use(livereload({port: livereloadport}));
// Use our 'dist' folder as rootfolder
server.use(express.static('./dist'));
// Because I like HTML5 pushstate .. this redirects everything back to our index.html
server.all('/*', function(req, res) {
    res.sendfile('index.html', { root: 'dist' });
});

// Dev task
gulp.task('dev', ['clean', 'views', 'stylus', 'lint', 'browserify', 'copy-files'], function() { });

// Clean task
gulp.task('clean', function(cb) {
    del('dist/views', cb);
});

// JSHint task
gulp.task('lint', function() {
    gulp.src('app/scripts/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Browserify task
gulp.task('browserify', function() {
    // Single point of entry (make sure not to src ALL your files, browserify will figure it out)
    gulp.src(['app/scripts/main.js'])
        .pipe(browserify({
            insertGlobals: true,
            debug: false
        }))
        // Bundle to a single file
        .pipe(concat('bundle.js'))
        // Output it to our dist folder
        .pipe(gulp.dest('dist/js'));
});

// Views task
gulp.task('views', function() {
    // Get our index.html

    gulp.src('app/index.jade')
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest('dist/'));

    // Any other view files from app/views
    gulp.src('app/views/*.jade')
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest('dist/views/'));
});

gulp.task('stylus', function() {
    gulp.src('app/styles/main.styl')
        .pipe(stylus({ pretty: true }))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('watch',  function() {
    // Start webserver
    server.listen(serverport);
    // Start live reload
    refresh.listen(livereloadport);

    // Watch our scripts, and when they change run lint and browserify
    gulp.watch(['app/scripts/*.js', 'app/scripts/**/*.js'],[
        'lint',
        'browserify'
    ]);
    // Watch our sass files
    gulp.watch(['app/styles/**/*.styl'], [
        'stylus'
    ]);

    gulp.watch(['app/**/*.jade'], [
        'views'
    ]);

    gulp.watch('./dist/**').on('change', refresh.changed);

});

gulp.task('copy-files', ['lint'], function() {
    gulp.src('app/assets/css/*.css')
        .pipe(gulp.dest('dist/css'));

    gulp.src('app/assets/js/*.js')
        .pipe(gulp.dest('dist/js'));

    gulp.src('app/assets/img/*.*')
        .pipe(gulp.dest('dist/img'));

    gulp.src('app/assets/fonts/*.*')
        .pipe(gulp.dest('dist/fonts'));

    gulp.src('app/favicon.ico')
        .pipe(gulp.dest('dist'));
});


gulp.task('default', ['dev', 'watch']);
