'use strict';

var angular = require('angular');

var WelcomeController = require('./controllers/welcome.js');

var app = angular.module('scaffold.app', []);
app.controller('WelcomeCtrl', ['$scope', WelcomeController]);
