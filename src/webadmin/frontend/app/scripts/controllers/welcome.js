'use strict';

var WelcomeController = function($scope) {
    $scope.greeting = 'Mezquite Labs angular scaffold.';
};

module.exports = WelcomeController;
