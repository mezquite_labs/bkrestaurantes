'use strict';
var angular = require('angular');

angular.module('scaffold.app').config([
    '$stateProvider',
    function ($stateProvider) {
        $stateProvider
            .state({
                name: 'home',
                url: '/',
                templateUrl: 'index.html',
                controller: 'WelcomeCtrl'
            });
    }
]);
