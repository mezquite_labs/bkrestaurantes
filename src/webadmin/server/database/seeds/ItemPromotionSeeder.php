<?php

use Illuminate\Database\Seeder;

class ItemPromotionSeeder extends Seeder
{
    public function run()
    {
        factory(App\ItemPromotion::class, 3)->create();
    }
}