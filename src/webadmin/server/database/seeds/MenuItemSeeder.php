<?php

use Illuminate\Database\Seeder;

class MenuItemSeeder extends Seeder
{
    public function run()
    {
        factory(App\MenuItem::class, 3)->create();
    }
}