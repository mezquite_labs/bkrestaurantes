<?php

$factory->define(App\Promotion::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->sentence(10)
    ];
});