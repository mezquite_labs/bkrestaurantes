<?php

$factory->define(App\ItemPromotion::class, function (Faker\Generator $faker) use ($factory){
    return [
        'menu_item_id' => $factory->create(App\MenuItem::class)->id,
        'promotion_id' => $factory->create(App\Promotion::class)->id,
        'beginning' => $faker->dateTimeBetween('-1 days', 'now'),
        'ending' => $faker->dateTimeBetween('1 days','1 years')
    ];
});
