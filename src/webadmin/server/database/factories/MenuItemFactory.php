<?php

$factory->define(App\MenuItem::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence(7),
        'price' => $faker->randomFloat(),
        'isAvailable' => true,
    ];
});
