<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_promotions', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_item_id')
                ->unsigned();
            $table->integer('promotion_id')
                ->unsigned();

            $table->dateTime('beginning');
            $table->dateTime('ending');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('menu_item_id')
                ->references('id')
                ->on('menu_items')
                ->onDelete('cascade');;
            $table->foreign('promotion_id')
                ->references('id')
                ->on('promotions')
                ->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_promotions', function (Blueprint $table) {
            $table->dropForeign('item_promotions_menu_item_id_foreign');
            $table->dropForeign('item_promotions_promotion_id_foreign');
        });
        Schema::drop('item_promotions');
    }
}
