##crud generator uses laracast generators as:

create_dogs_table --schema="name:string"

So any of these will do:

username:string
body:text
age:integer
published_at:date
excerpt:text:nullable
email:string:unique:default('foo@example.com')

php artisan make:crud menu_items --schema="id:increments,name:string,price:decimal,description:string,isAvailable:boolean"
php artisan make:crud promotions --schema="id:increments,description:string,isActive:boolean"
php artisan make:crud item_promotions --schema="menu_item_id:integer:foreign,promotion_id:integer:foreign,beginning:dateTime,ending:dateTime"

*id, increments is sete by default.