<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemPromotion extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'item_promotions';

    protected $fillable = ['menu_item_id', 'promotion_id', 'beginning', 'ending'];
}
