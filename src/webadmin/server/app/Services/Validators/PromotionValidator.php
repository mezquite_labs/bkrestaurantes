<?php namespace App\Services\Validators;

class PromotionValidator extends AbstractValidator
{
    protected $rules = array(
        'description' =>'required'
    );
}
