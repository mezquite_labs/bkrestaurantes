<?php namespace App\Services\Validators;

class MenuItemValidator extends AbstractValidator
{
    protected $rules = array(
        'name' =>'required',
        'description' => 'required',
        'price' => 'required|regex:/^\d*(\.\d{2})?$/'
    );
}
