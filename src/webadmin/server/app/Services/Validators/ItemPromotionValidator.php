<?php namespace App\Services\Validators;

class ItemPromotionValidator extends AbstractValidator
{
    protected $rules = array(
        'menu_item_id' =>'required|exists:menu_items,id',
        'promotion_id' =>'required|exists:promotions,id',
        'beginning' =>'required|date|before:ending',
        'ending' =>'required|date'
    );
}
