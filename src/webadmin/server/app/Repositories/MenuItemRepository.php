<?php namespace App\Repositories;

use App\MenuItem;
use App\Services\Validators\MenuItemValidator;

class MenuItemRepository extends AbstractRepository {

    public function __construct(MenuItem $entity, MenuItemValidator $validator) {
        parent::__construct($entity, $validator);
    }
}
