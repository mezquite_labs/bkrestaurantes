<?php namespace App\Repositories;

use App\ItemPromotion;
use App\Services\Validators\ItemPromotionValidator;

class ItemPromotionRepository extends AbstractRepository {

    public function __construct(ItemPromotion $entity, ItemPromotionValidator $validator) {
        parent::__construct($entity, $validator);
    }
}
