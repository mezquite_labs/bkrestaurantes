<?php namespace App\Repositories;

use App\Promotion;
use App\Services\Validators\PromotionValidator;

class PromotionRepository extends AbstractRepository {

    public function __construct(Promotion $entity, PromotionValidator $validator) {
        parent::__construct($entity, $validator);
    }
}
