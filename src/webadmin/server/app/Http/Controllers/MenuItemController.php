<?php namespace App\Http\Controllers;

use App\Repositories\MenuItemRepository;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory as Response;

class MenuItemController extends AbstractController {

    public function __construct(Request $request, Response $response, MenuItemRepository $repository) {
        parent::__construct($request, $response, $repository);
    }
}
