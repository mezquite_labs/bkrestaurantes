<?php namespace App\Http\Controllers;

use App\Repositories\ItemPromotionRepository;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory as Response;

class ItemPromotionController extends AbstractController {

    public function __construct(Request $request, Response $response, ItemPromotionRepository $repository) {
        parent::__construct($request, $response, $repository);
    }
}
