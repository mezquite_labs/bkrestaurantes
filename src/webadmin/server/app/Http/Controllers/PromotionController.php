<?php namespace App\Http\Controllers;

use App\Repositories\PromotionRepository;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory as Response;

class PromotionController extends AbstractController {

    public function __construct(Request $request, Response $response, PromotionRepository $repository) {
        parent::__construct($request, $response, $repository);
    }
}
