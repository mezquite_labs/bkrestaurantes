<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api'], function()
{
    $this->resource('menu_item', 'MenuItemController');
    $this->resource('promotion', 'PromotionController');
    $this->resource('item_promotion', 'ItemPromotionController');
});




